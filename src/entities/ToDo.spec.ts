import ToDo from './ToDo';

const TEST_TITLE = "Read about typescript";
const TEST_CONTENT = "I must read about typescript language";

describe('ToDo class', () => {

    test('Should instantiate a Todo', () => {
        expect(new ToDo(TEST_TITLE, TEST_CONTENT)).toBeInstanceOf(ToDo);
    });

    test('Should get the title of a Todo', () => {
        const todo = new ToDo(TEST_TITLE, TEST_CONTENT);
        expect(todo.getTitle()).toBe(TEST_TITLE);
    });

    test('Should get the content of a Todo', () => {
        const todo = new ToDo(TEST_TITLE, TEST_CONTENT);
        expect(todo.getContent()).toBe(TEST_CONTENT);
    });

    test('Should get the status of a new Todo as false', () => {
        const todo = new ToDo(TEST_TITLE, TEST_CONTENT);
        expect(todo.isDone()).toBe(false);
    });

    test('Should set the status of a Todo to true', () => {
        const todo = new ToDo(TEST_TITLE, TEST_CONTENT);
        expect(todo.isDone()).toBe(false);
        todo.do();
        expect(todo.isDone()).toBe(true);
    });
});

