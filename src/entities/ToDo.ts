class ToDo {
    private title: string;
    private content: string;
    private done: boolean;

    constructor(title: string, content: string) {
        this.title = title;
        this.content = content;
        this.done = false;
    }

    public getTitle(): string {
        return this.title;
    }

    public getContent(): string {
        return this.content;
    }

    public isDone(): boolean {
        return this.done;
    }

    public do() {
        this.done = true;
    }
}

export default ToDo;