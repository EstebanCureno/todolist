import ToDo from "./ToDo";

class ToDoList {
    private toDos: ToDo[];

    constructor() {
        this.toDos = [];
    }

    public getToDos(): ToDo[] {
        return this.toDos;
    }

    public push(toDo: ToDo) {
        this.toDos.push(toDo);
    }
}
export default ToDoList;