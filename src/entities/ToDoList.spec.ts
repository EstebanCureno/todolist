import ToDoList from "./ToDoList";
import ToDo from "./ToDo";

const TEST_TITLE = "Read about typescript";
const TEST_CONTENT = "I must read about typescript language";
let testToDo: ToDo;

describe('ToDoList', () => {
   beforeEach(() => {
      testToDo = new ToDo(TEST_TITLE, TEST_CONTENT);
   });
   it('Should Instantiate a ToDoList', () => {
      expect(new ToDoList()).toBeInstanceOf(ToDoList);
   });

   it('Should get empty array if list is new', () => {
      const toDoList = new ToDoList();
      expect(toDoList.getToDos()).toEqual([]);
   });

   it('Should add ToDos to the list and get the list', () => {
      const toDoList = new ToDoList();
      toDoList.push(testToDo);
      expect(toDoList.getToDos()).toEqual([testToDo]);
      toDoList.push(testToDo);
      expect(toDoList.getToDos()).toEqual([testToDo, testToDo]);
   });

});