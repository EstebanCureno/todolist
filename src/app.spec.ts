import { app } from './app';

test('should return hello world', () => {
  expect(app()).toBe('hello world');
});