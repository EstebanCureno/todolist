const { src, parallel} = require('gulp')
const  tslint = require('gulp-tslint')

function lint(){
    return src(['./src/**/**.ts'])
    .pipe(tslint({
        formatter: "verbose"
    }))
    .pipe(tslint.report())
}

exports.default = parallel(lint)