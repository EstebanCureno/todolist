# To Do List

Just a simple app to keep track of a **daily todo list**

## General Objective
* To keep track of already done daily activities.

### User stories

| As a |I  want| so that |done?|
|-|--|--|--|
| Human | to see all my todos of the day |can decide wich one I want to do next|NO|
| Human | to put a todo item in the list |I can keep track of it trough the day|NO|
| Human | to check an already done todo |I can focus on another todo|NO|
| Human | to see all my already done todos |I can keep track of already done todos |NO|


### Class diagram

```mermaid
classDiagram
    class ToDoList
       ToDoList : +Todo[] Todos
       ToDoList : +getTodos()
       ToDoList : +addTodo()
    class ToDo
       ToDo: -Bool checked
       ToDo: -String name 
       ToDo: -String description
       ToDo: +Todo(String name, string description)
       ToDo: +setCheck(boolean)
       ToDo: +getName()
       ToDo: +getDescription()
       ToDo: +getCheck()
    ToDoList *-- ToDo   
```